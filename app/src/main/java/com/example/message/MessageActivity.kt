package com.example.message

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.repackaged.com.google.common.base.Objects
import com.example.home.User
import com.example.loginfirebase.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_message.*
import java.util.*
import kotlin.collections.HashMap


class MessageActivity : AppCompatActivity() {
    private lateinit var mAdapterChat : MessageAdapter
    private val listChat = ArrayList<Chat>()
    private val firebaseData = FirebaseAuth.getInstance()
    private val dataDetail : FirebaseDatabase = FirebaseDatabase.getInstance()
    private val userMessage : FirebaseUser = firebaseData.currentUser!!
    private lateinit var references : DatabaseReference
    private lateinit var seenListener : ValueEventListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)
        setSupportActionBar(tool_bar_detail)
        val action = supportActionBar
        action?.let {
            it.setDisplayHomeAsUpEnabled(true)

        }
        val idDetail = intent.getStringExtra("userId")
        references = dataDetail.getReference("User").child(idDetail.toString())

        btn_send.setOnClickListener {
            if (edt_send.text.toString() != ""){
                sendMessage(userMessage.uid, idDetail.toString(), edt_send.text.toString())
            }else{
                Toast.makeText(this, "không gửi được tin nhắn!", Toast.LENGTH_SHORT).show()
            }
            edt_send.setText("")
        }
        references.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val user : User? = snapshot.getValue(User::class.java)
                tv_name_detail.text = user?.userName
                if(user?.status == "online"){
                    status.text = "online"
                }else{
                    status.text = "offline"
                }
                if(user?.imageUrl.equals("default")){
                    img_detail_user.setImageResource(R.drawable.avt_clone)
                }else{
                    Glide.with(applicationContext).load(user?.imageUrl).into(img_detail_user)
                }
                    readMessage(userMessage.uid, idDetail.toString(), user?.imageUrl.toString())
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "error detail: ${error.message}")
            }
        })
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    private fun sendMessage(sender : String, receiver : String, message : String){
        val messageDatabase : FirebaseDatabase = FirebaseDatabase.getInstance()
        references  = messageDatabase.reference
        val hashMessage : HashMap<String, Any> = HashMap()
        hashMessage["receiver"] = receiver
        hashMessage["sender"] = sender
        hashMessage["message"] = message
        hashMessage["isSeen"] = false
        references.child("chats").push().setValue(hashMessage)
    }
    private fun readMessage(myId : String, userId : String, imgUrl : String){
        references = dataDetail.getReference("chats")
        references.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                listChat.clear()
                snapshot.children.forEach {
                    val chat : Chat?= it.getValue(Chat::class.java)
                    if (chat?.receiver.equals(myId) && chat?.sender.equals(userId)
                            || chat?.sender.equals(myId) && chat?.receiver.equals(userId)){
                        if (chat != null) {
                            listChat.add(chat)
                            mAdapterChat = MessageAdapter(imgUrl)
                            mAdapterChat.submitList(listChat)
                            rcv_detail.apply {
                                val linearLayoutManager : LinearLayoutManager = LinearLayoutManager(this@MessageActivity)
                                linearLayoutManager.stackFromEnd = true
                                layoutManager = linearLayoutManager
                                adapter = mAdapterChat
                            }


                        }

                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }
    private fun status(status : String) {
        references = dataDetail.getReference("User").child(userMessage.uid)
        val hashUser : java.util.HashMap<String, Any> = java.util.HashMap()
        hashUser["status"] = status
        references.updateChildren(hashUser)
    }

    override fun onResume() {
        super.onResume()
        status("online")

    }

    override fun onPause() {
        super.onPause()
        status("offline")
    }

    override fun onStop() {
        super.onStop()
        status("offline")
    }

}