package com.example.message

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.loginfirebase.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.demo_receiver.view.*
import kotlinx.android.synthetic.main.demo_senders.view.*

class MessageAdapter(val imgUrl : String) : ListAdapter<Chat, ViewHolderChat>(DiffCallBackChat()){
    private lateinit var userChat : FirebaseUser
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderChat {
        return when(viewType){
              TYPE_SEND -> ViewHolderSend(
                      LayoutInflater.from(parent.context).inflate(R.layout.demo_senders, parent, false)
              )else -> ViewHolderReceiver(
                    LayoutInflater.from(parent.context).inflate(R.layout.demo_receiver, parent, false)

            )
        }
    }

    override fun onBindViewHolder(holder: ViewHolderChat, position: Int) {
        holder.binDataChat(getItem(position), imgUrl)
    }
    override fun getItemViewType(position: Int): Int {
        userChat = FirebaseAuth.getInstance().currentUser!!
        return if (getItem(position).sender == userChat.uid){
            TYPE_SEND
        }else{
            TYPE_RECEIVER
        }
    }
}
abstract class ViewHolderChat(itemView: View) : RecyclerView.ViewHolder(itemView){
    abstract fun binDataChat(chat : Chat, img : String)
}

class ViewHolderSend(itemView: View) : ViewHolderChat(itemView){
    override fun binDataChat(chat: Chat, img: String) {
        itemView.run {
            show_message.text = chat.message
        }
    }
}
class ViewHolderReceiver(itemView: View) : ViewHolderChat(itemView){
    override fun binDataChat(chat: Chat, img: String) {
        itemView.run {
            tv_receiver.text = chat.message
            if (img == "default"){
                profile_img.setImageResource(R.drawable.avt_clone)
            }else{
                Glide.with(this).load(img).into(profile_img)
            }
        }
    }
}
class DiffCallBackChat : DiffUtil.ItemCallback<Chat>(){
    override fun areItemsTheSame(oldItem: Chat, newItem: Chat): Boolean {
        return oldItem.id == newItem.id
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Chat, newItem: Chat): Boolean {
       return oldItem == newItem
    }

}
const val TYPE_SEND = 1
const val TYPE_RECEIVER = 2