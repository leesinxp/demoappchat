package com.example.home

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.fragment.FragmentChat
import com.example.loginfirebase.R
import com.example.message.MessageActivity
import kotlinx.android.synthetic.main.demo_list.view.*
import java.util.*

class UserAdapter(val isChat : Boolean) : ListAdapter<User, ViewHolderUser>(DiffCallBackUser()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderUser {
        return ViewHolderUser(
                LayoutInflater.from(parent.context).inflate(R.layout.demo_list, parent, false)

        )
    }

    override fun onBindViewHolder(holder: ViewHolderUser, position: Int) {
        holder.binData(getItem(position), isChat)
    }

}
class ViewHolderUser(itemView : View) : RecyclerView.ViewHolder(itemView){
      fun binData(user: User, isChatUser : Boolean){
          itemView.run {
              tv_name.text = user.getUserName()
              if (user.getImageUrl().equals("default")){
                  img_user_tab.setImageResource(R.drawable.avt_clone)
              }else{
                  Glide.with(context).load(user.imageUrl).into(img_user_tab)
              }
              setOnClickListener {
                  val intent = Intent(context, MessageActivity::class.java)
                  intent.putExtra("userId", user.getId())
                  context.startActivity(intent)
              }
              if (isChatUser){
                  if (user.getStatus().equals("online")){
                      img_status.setImageResource(R.drawable.ic_online)
                  }else{
                      img_status.setImageResource(R.drawable.ic_offline)
                  }

              }
          }
      }
}
class DiffCallBackUser : DiffUtil.ItemCallback<User>(){
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.idItem == newItem.idItem
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return  oldItem == newItem
    }

}