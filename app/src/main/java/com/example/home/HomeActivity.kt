package com.example.home

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.loginfirebase.MainActivity
import com.example.loginfirebase.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.diaglog_out.*
import java.util.HashMap

class HomeActivity : AppCompatActivity() {
    private val TIME_DELAY = 1000
    private val firebaseDatas : FirebaseDatabase = FirebaseDatabase.getInstance()
    private lateinit var reference : DatabaseReference
    private lateinit var firebaseUser : FirebaseUser
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(tool_bar)
        vp_app.adapter = ViewPagerAdapter(supportFragmentManager)
        tabLayout.setupWithViewPager(vp_app)
        log_out.setOnClickListener {
            showDialog()
        }
        firebaseUser = FirebaseAuth.getInstance().currentUser!!
        reference = firebaseDatas.getReference("User").child(firebaseUser.uid)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val user : User? = snapshot.getValue(User::class.java)
                tv_user_name.text = user?.getUserName()
                if (user?.getImageUrl().equals("default")) {
                    profile_image.setImageResource(R.drawable.avt_clone)
                } else {
                    Glide.with(applicationContext).load(user?.imageUrl).into(profile_image)
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }
    private fun showDialog(){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.diaglog_out)
        dialog.setCancelable(false)
        val yesBtn = dialog.findViewById<Button>(R.id.btn_out)
        val progress = dialog.findViewById<ProgressBar>(R.id.progress_home)
        yesBtn.setOnClickListener {
            progress.visibility = View.VISIBLE
            Handler().postDelayed({
                startActivity(Intent(this, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
            }, TIME_DELAY.toLong())
        }
        dialog.show()
        val cancel = dialog.findViewById<Button>(R.id.btn_cancel)
        cancel.setOnClickListener {
            dialog.cancel()
        }

    }
    private fun status(status : String){
        reference = firebaseDatas.getReference("User").child(firebaseUser.uid)
        val hashUser : HashMap<String, Any> = HashMap()
        hashUser["status"] = status
        reference.updateChildren(hashUser)
    }

    override fun onResume() {
        super.onResume()
        status("online")
    }

    override fun onPause() {
        super.onPause()
        status("offline")
    }


}
