package com.example.home;

import java.util.UUID;

public class User {
    String idItem = UUID.randomUUID().toString();
    String id;
    String userName;
    String imageUrl;
    String status;
    String imageUrlPro;

    public User(){}

    public String getIdItem() {
        return idItem;
    }

    public void setIdItem(String idItem) {
        this.idItem = idItem;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImageUrlPro() {
        return imageUrlPro;
    }

    public void setImageUrlPro(String imageUrlPro) {
        this.imageUrlPro = imageUrlPro;
    }

    public User(String idItem, String id, String userName, String imageUrl, String status, String imageUrlPro) {
        this.idItem = idItem;
        this.id = id;
        this.userName = userName;
        this.imageUrl = imageUrl;
        this.status = status;
        this.imageUrlPro = imageUrlPro;
    }
}
