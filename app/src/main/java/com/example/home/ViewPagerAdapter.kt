package com.example.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.fragment.FragmentChat
import com.example.fragment.FragmentProfile
import com.example.fragment.FragmentUser

class ViewPagerAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getCount(): Int {
       return 3
    }

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 ->{
                FragmentChat()
            }
            1 ->{
                FragmentUser()
            }
            2 ->{
                FragmentProfile()
            }
            else ->{
                FragmentChat()
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title = ""
        title = when(position){
            0 -> "Tin nhắn"
            1 -> "Mọi người"
            2 -> "Profile"
            else -> "Tin nhắn"
        }
        return title
    }

}