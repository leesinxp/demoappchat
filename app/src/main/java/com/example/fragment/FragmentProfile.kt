package com.example.fragment

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.ContentResolver
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.fragment.app.Fragment
import bolts.Continuation
import bolts.Task
import com.bumptech.glide.Glide
import com.example.home.User
import com.example.loginfirebase.R
import com.google.android.gms.auth.api.signin.internal.Storage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.*
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.IOException
import java.util.*
import kotlin.collections.HashMap


class FragmentProfile : Fragment() {
    private lateinit var imageUri : Uri
    private lateinit var storageReferences : StorageReference
    private val firebaseAcc : FirebaseAuth = FirebaseAuth.getInstance()
    private val userProfile : FirebaseUser = firebaseAcc.currentUser!!
    private val data : FirebaseDatabase = FirebaseDatabase.getInstance()
    private lateinit var reference : DatabaseReference
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        storageReferences = FirebaseStorage.getInstance().getReference("uploads")
        reference = data.getReference("User").child(userProfile.uid)
        img_profile.setOnClickListener {
            openImage()
        }
        reference.addValueEventListener(object : ValueEventListener {
            @SuppressLint("CheckResult")
            override fun onDataChange(snapshot: DataSnapshot) {
                val user: User? = snapshot.getValue(User::class.java)
                tv_name_profile?.text = user?.userName
                Glide.with(this@FragmentProfile).load(user?.imageUrl).into(img_profile)

            }

            override fun onCancelled(error: DatabaseError) {

            }


        })

    }




    private fun openImage(){
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.action = Intent.ACTION_OPEN_DOCUMENT
        startActivityForResult(Intent.createChooser(intent, "Select picture"), IMAGE_REQUEST)
    }
    private fun getFileExtension(img : Uri): String? {
        val contentResolver : ContentResolver = context?.contentResolver!!
        val mimeType : MimeTypeMap = MimeTypeMap.getSingleton()
        return mimeType.getExtensionFromMimeType((contentResolver.getType(img)))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK
            && data != null && data.data != null){
            imageUri = data.data!!
            try {
                upLoadImage(imageUri)

            }catch (e: IOException){
                e.printStackTrace()
            }
        }
    }


    private fun upLoadImage(img : Uri){
        val pd : ProgressDialog = ProgressDialog(context)
        pd.setMessage("Đang tải")
        pd.show()

        val fireReferences : StorageReference = storageReferences.child("" + System.currentTimeMillis() +getFileExtension(imageUri))
        fireReferences.putFile(img)
            .addOnSuccessListener {
                pd.dismiss()
                Toast.makeText(context, "Tải ảnh thành công", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener {
                pd.dismiss()
                Toast.makeText(context, "Không tải được ảnh!", Toast.LENGTH_SHORT).show()
            }
            .addOnProgressListener {
                val progress: Double = 100.0 * it.bytesTransferred / it
                    .totalByteCount
                pd.setMessage("Đang tải " + progress.toInt() + "%")

            }
         fireReferences.putFile(imageUri).continueWithTask { task ->
            if (!task.isSuccessful){
                task.exception?.let {
                    throw  it
                }
            }
            fireReferences.downloadUrl
        }.addOnCompleteListener {task ->
            if (task.isSuccessful){
                val downloadUri = task.result
                val mUri = downloadUri.toString()
                reference = data.getReference("User").child(userProfile.uid)
                val hashMap : HashMap<String, Any> = HashMap()
                hashMap["imageUrl"] = ""+mUri
                reference.updateChildren(hashMap)
                pd.dismiss()
            }else{
                pd.dismiss()
                Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
            }

        }

    }


}
const val IMAGE_REQUEST = 1