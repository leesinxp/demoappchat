package com.example.fragment

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.home.User
import com.example.home.UserAdapter
import com.example.loginfirebase.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_user.*
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.properties.Delegates


class FragmentUser : Fragment() {
    private var listUserFrag = ArrayList<User>()
    private var isChat by Delegates.notNull<Boolean>()
    private lateinit var mAdapterTab : UserAdapter
    private var limit = 0
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refresh_data.setOnRefreshListener {
            listUser(true)
            refresh_data.isRefreshing = false
        }

    }
    init {
        listUser(true)
    }
    private fun listUser(isRefresh : Boolean) {
        val firebaseUser : FirebaseUser = FirebaseAuth.getInstance().currentUser!!
        val data : DatabaseReference = FirebaseDatabase.getInstance().reference
        val dataAdd : DatabaseReference = data.child("User")
        dataAdd.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                listUserFrag.clear()
                if (isRefresh) {
                    limit
                }
                    snapshot.children.forEach {
                        val user: User? = it.getValue(User::class.java)
                        if (!user?.id?.equals(firebaseUser.uid)!!) {
                            try {
                                listUserFrag.add(user)
                                mAdapterTab = UserAdapter(true)
                                limit += listUserFrag.size
                                mAdapterTab.submitList(listUserFrag)
                                rcv_user.apply {
                                    adapter = mAdapterTab
                                    layoutManager = LinearLayoutManager(context)
                                }
                            }catch (e : IOException){
                                e.printStackTrace()
                                Log.w(TAG, "error : ${e.message}")
                            }


                        }
                    }
                    Log.d("kk", listUserFrag.size.toString())



            }

            override fun onCancelled(error: DatabaseError) {
                 Log.w(TAG, "error: ${error.message}")
            }

        })
    }








}