package com.example.loginfirebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_reset_password.*

class ResetPasswordActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        resetPass()
    }
    private fun resetPass(){
        val authReset : FirebaseAuth = FirebaseAuth.getInstance()
            btn_reset.setOnClickListener {
                progress_resert.visibility = View.VISIBLE
                if (reset_mail.text.toString() == "") {
                    Toast.makeText(this, "Vui lòng nhập email!", Toast.LENGTH_SHORT).show()
                    progress_resert.visibility = View.GONE
                }else{
                authReset.sendPasswordResetEmail(reset_mail?.text.toString())
                    .addOnCompleteListener(this) { p0 ->
                        if (p0.isSuccessful) {
                            Toast.makeText(this@ResetPasswordActivity, "Chúng tôi đã gửi cho bạn hướng dẫn để đặt lại mật khẩu của bạn", Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(this@ResetPasswordActivity, "không gửi được email!", Toast.LENGTH_SHORT).show()
                        }
                        progress_resert.visibility = View.GONE
                    }
                }
        
       }
    }
}