package com.example.loginfirebase

import android.app.Dialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.Toast
import androidx.core.content.edit
import com.example.home.HomeActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_save_data.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN )
        sign_up.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
        }
        forgot_pass.setOnClickListener {
            startActivity(Intent(this, ResetPasswordActivity::class.java))
        }
        loadData()
        logIn()
        }
    private fun logIn(){
        val authLogin : FirebaseAuth = FirebaseAuth.getInstance()

        btn_login.setOnClickListener {
            if (check_box.isChecked){
                saveData(edt_mail_log_in.text.toString(), edt_pass_log_in.text.toString())
            }else
                clearData()
            progressBar_login.visibility = View.VISIBLE
            if(edt_mail_log_in.text.toString().trim() == ""){
                Toast.makeText(this, "Vui lòng nhập email!", Toast.LENGTH_SHORT).show()
                progressBar_login.visibility = View.GONE
            }else if(edt_pass_log_in.text.toString().trim() == ""){
                Toast.makeText(this, "Vui lòng nhập mật khẩu!", Toast.LENGTH_SHORT).show()
                progressBar_login.visibility = View.GONE
            }else{
                authLogin.signInWithEmailAndPassword(
                        edt_mail_log_in?.text.toString().trim(),
                        edt_pass_log_in?.text.toString().trim()
                ).addOnCompleteListener(this) {taskLogin ->
                    Toast.makeText(this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show()
                    progressBar_login.visibility = View.GONE
                    if(!taskLogin.isSuccessful){
                        Toast.makeText(this, "Thông tin tài khoản mật khẩu không chính xác!", Toast.LENGTH_SHORT).show()
                    }else{
                        val intent = Intent(this, HomeActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        writeNewUser(edt_mail_log_in.text.toString())
                        startActivity(intent)
                        finish()
                    }
                }

            }
        }
    }

    override fun onResume() {
        super.onResume()
        progressBar_login.visibility = View.GONE
    }
    private fun writeNewUser(name : String){
        val Firebase : FirebaseDatabase = FirebaseDatabase.getInstance()
        val databases : DatabaseReference = Firebase.getReference("account")
        val users = Account(name)
        databases.setValue(users)
        databases.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val userValue = snapshot.value
                Log.d(TAG, "user is: $userValue")
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, "Failed to read value.", error.toException())
            }

        })
    }
    private fun saveData(userName : String, password : String){
        val pref : SharedPreferences = getSharedPreferences("PREF_ACC", Context.MODE_PRIVATE)
        val editor : SharedPreferences.Editor = pref.edit()
        editor.putString(USER_NAME, userName)
        editor.putString(PASS_WORD, password)
        editor.putBoolean(REMEMBER, check_box.isChecked)
        editor.apply()
    }
    private fun clearData(){
        val pref : SharedPreferences = getSharedPreferences("PREF_ACC", Context.MODE_PRIVATE)
        val edit : SharedPreferences.Editor = pref.edit()
        edit.clear()
        edit.apply()
    }
    private fun loadData(){
        val pref : SharedPreferences = getSharedPreferences("PREF_ACC", Context.MODE_PRIVATE)
        if (pref.getBoolean(REMEMBER, false)){
           edt_mail_log_in.setText(pref.getString(USER_NAME, ""))
           edt_pass_log_in.setText(pref.getString(PASS_WORD, ""))
            check_box.isChecked = true
        }else{
            check_box.isChecked = false
        }
    }

    }
const val USER_NAME = "USER"
const val PASS_WORD = "PASS"
const val REMEMBER = "remember"
