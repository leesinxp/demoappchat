package com.example.loginfirebase

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.example.home.HomeActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private val firebaseData : FirebaseDatabase = FirebaseDatabase.getInstance()
    private lateinit var references : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        sign_in.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN )
        checkSignUp()
        signUp()

    }
    private fun checkSignUp(){
        textInputLayout_email_signup.editText?.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length == 0){
                    textInputLayout_email_signup.error = "Không được để trống email"
                    textInputLayout_email_signup.isErrorEnabled = true
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        textInputLayout_pass_signup.editText?.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length == 0){
                    textInputLayout_pass_signup.error = "không được để trống mật khẩu"
                    textInputLayout_pass_signup.isErrorEnabled = true
                }else if (s?.length!! < 5){
                    textInputLayout_pass_signup.error = "mật khẩu của bạn quá yếu "
                    textInputLayout_pass_signup.isErrorEnabled = true
                }else if (s.isEmpty()){
                    textInputLayout_pass_signup_rework.error = "không được để trống mật khẩu"
                } else{
                    textInputLayout_pass_signup.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        textInputLayout_pass_signup_rework.editText?.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length == 0){
                    textInputLayout_pass_signup_rework.error = "Vui lòng nhập mật khẩu"
                    textInputLayout_pass_signup_rework.isErrorEnabled = true
                }else if (s?.length!! < 5){
                    textInputLayout_pass_signup_rework.error = "mật khẩu của bạn quá yếu"
                    textInputLayout_pass_signup_rework.isErrorEnabled = true
                }else if (s.isEmpty()){
                    textInputLayout_pass_signup_rework.error = "không được để trống mật khẩu"
                }
                else{
                    textInputLayout_pass_signup_rework.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
    }
    private fun signUp() {
        val auth: FirebaseAuth = FirebaseAuth.getInstance()
        btn_signup.setOnClickListener {
            progressbar.visibility = View.VISIBLE
            if (userName.text.toString().trim() == "") {
                Toast.makeText(this, "Không được để trống tên đăng nhập!", Toast.LENGTH_SHORT).show()
                progressbar.visibility = View.GONE
            } else if (edt_pass_sign_up.text.toString().trim() == "") {
                Toast.makeText(this, "Vui lòng nhập mật khẩu!", Toast.LENGTH_SHORT).show()
                progressbar.visibility = View.GONE
            } else if (edt_pass_sign_up.text.toString().trim().length < 5) {
                Toast.makeText(this, "Mật khẩu quá ngắn không an toàn", Toast.LENGTH_SHORT).show()
                progressbar.visibility = View.GONE
            } else if (edt_pass_sign_up.text.toString().trim() != edt_pass_rework.text.toString().trim()){
                Toast.makeText(this, "Mật khẩu không trùng khớp!", Toast.LENGTH_SHORT).show()
                progressbar.visibility = View.GONE
            } else if(edt_mail_sign_up.text.toString().trim() == ""){
                Toast.makeText(this, "Không được để trống gmail!", Toast.LENGTH_SHORT).show()
                progressbar.visibility = View.GONE
            } else {
                //create account user
                auth.createUserWithEmailAndPassword(
                        edt_mail_sign_up?.text.toString().trim(),
                        edt_pass_sign_up?.text.toString().trim()
                )

                        .addOnCompleteListener(
                                this
                        ) { p0 ->
                            if (p0.isSuccessful) {

                                val firebaseUser : FirebaseUser? = auth.currentUser
                                val userId = firebaseUser?.uid
                                //write to database
                                references = firebaseData.getReference("User").child(userId.toString())
                                val hashMap : HashMap<String, String> = HashMap()
                                hashMap["id"] = userId.toString()
                                hashMap["userName"] = userName.text.toString()
                                hashMap["imageUrl"] = "default"
                                hashMap["imageUrlPro"] = "default"

                                references.setValue(hashMap).addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        val intent = Intent(this@SignUpActivity, ContentLogIn::class.java)
                                        Toast.makeText(
                                                this@SignUpActivity,
                                                "Đăng ký thành công",
                                                Toast.LENGTH_SHORT
                                        ).show()
                                        progressbar.visibility = View.GONE
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                        startActivity(intent)
                                        finish()
                                    }
                                }

                            } else {
                                Toast.makeText(
                                        this@SignUpActivity,
                                        "Đăng ký thất bại!",
                                        Toast.LENGTH_SHORT
                                ).show()
                                progressbar.visibility = View.GONE
                            }
                        }
            }


        }
    }

    override fun onResume() {
        super.onResume()
        progressbar.visibility = View.GONE
    }
}