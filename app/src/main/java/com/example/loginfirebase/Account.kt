package com.example.loginfirebase

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Account(val email : String?= null)